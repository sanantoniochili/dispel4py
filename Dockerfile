FROM tiangolo/uwsgi-nginx:python3.7

LABEL maintainer="Sebastian Ramirez <tiangolo@gmail.com>"

# Install uWSGI
RUN pip install uwsgi

RUN apt-get update && apt-get install wget curl python-dev python-pip python3-pip python-setuptools git openmpi-bin openmpi-common libopenmpi-dev -y
RUN pip install mpi4py

# install dispel4py
RUN pip install git+git://github.com/dispel4py/dispel4py.git@master

# install numpy
RUN pip install numpy

# install flask
RUN pip install flask

ENV NGINX_WORKER_PROCESSES auto

COPY /app /app
