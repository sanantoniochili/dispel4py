import sys
import time
import types
from flask import Flask, request
from dispel4py.new import processor
from importlib import import_module
from dispel4py.new.processor import create_inputs, create_arg_parser
from dispel4py.workflow_graph import WorkflowGraph
from server_utils import simple_parser, module_parser, DynamicImporter
from dispel4py.new.processor import GenericWrapper, SimpleProcessingPE
from dispel4py.new.simple_process import process as simple_process

app = Flask(__name__)

@app.route('/simple', methods=['POST'])
# will implement a queue for client connection/edge per request
# new request for graph creation
def add_connection():
    # Save stdout to file
    original = sys.stdout
    filename = 'logs.out'
    sys.stdout = open(filename, 'w')

    # get raw data from curl as text
    text = request.get_data(as_text=True)
    words = text.split()
    
    # split string to list and feed to arg parser
    parser = simple_parser()
    args = parser.parse_args(words)

    imp = DynamicImporter()
    # e.g. curl -i -X POST -d '-fm dispel4py.examples.graph_testing.testing_PEs -fPE RandomWordProducer' http://localhost:8000
    from_node = imp.new_instance(args.fromModule,args.fromPE)
    to_node = imp.new_instance(args.toModule,args.toPE)

    graph = WorkflowGraph()
    graph.connect(from_node, args.fromConnection, to_node, args.toConnection) 
    graph.flatten()
    inputs = create_inputs(args, graph)

    elapsed_time = 0
    start_time = time.time()
    if graph is not None:
        simple_process(graph, inputs=inputs)

    print ("ELAPSED TIME: "+str(time.time()-start_time))

    sys.stdout.close()
    sys.stdout = original

    # Print any output to client's terminal
    with open("logs.out") as f:
        file_content = f.read()
    
    return file_content 


@app.route('/module', methods=['POST'])
def run_process():
    from importlib import import_module
    from dispel4py.new.processor import load_graph_and_inputs
    
    # Save stdout to file
    original = sys.stdout
    filename = 'logs.out'
    sys.stdout = open(filename, 'w')

    # get raw data from curl as text
    text = request.get_data(as_text=True)
    words = text.split()
    
    # split string to list and feed to arg parser
    parser = create_arg_parser()
    args = parser.parse_args(words)
    
    # same as dispel4py.new.simple-process
    graph, inputs = load_graph_and_inputs(args)

    elapsed_time = 0
    start_time = time.time()
    if graph is not None:
        simple_process(graph, inputs=inputs)

    print ("ELAPSED TIME: "+str(time.time()-start_time))

    sys.stdout.close()
    sys.stdout = original

    # Print any output to client's terminal
    with open("logs.out") as f:
        file_content = f.read()
    
    return file_content