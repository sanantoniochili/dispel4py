import argparse
from importlib import import_module

def simple_parser():  
    # parser to create edge on graph
    parser = argparse.ArgumentParser(
        description='Submit dispel4py nodes and connections.')
    parser.add_argument('-fm', '--fromModule', metavar='fromNode',
                        help='module with predefined source PE')
    parser.add_argument('-fPE', '--fromPE', metavar='fromPE',
                        help='predefined source PE class')
    parser.add_argument('-fC', '--fromConnection', metavar='fromConnection', type=str,
                        help='the name of the output of the source node \'fromNode\'')
    parser.add_argument('-tm', '--toModule', metavar='toNode',
                        help='module with predefined destination PE')
    parser.add_argument('-tPE', '--toPE', metavar='toPE',
                        help='predefined destination PE class')
    parser.add_argument('-tC', '--toConnection', metavar='toConnection', type=str,
                        help='the name of the input of the destination node \'toNode\'')
    parser.add_argument('-f', '--file', metavar='inputfile',
                        help='file containing input dataset in JSON format')
    parser.add_argument('-d', '--data', metavar='inputdata',
                        help='input dataset in JSON format')
    parser.add_argument('-i', '--iter', metavar='iterations', type=int,
                        help='number of iterations', default=1)
    return parser

def module_parser():
    parser = argparse.ArgumentParser(
        description='Submit dispel4py graph.')
    parser.add_argument('target', help='target execution platform')
    parser.add_argument('module', help='module that creates a dispel4py graph '
                        '(python module or file name)')
    return parser

class DynamicImporter:
    def new_instance(self, module_name, class_name):
        module = import_module(module_name)
        my_class = getattr(module, class_name)
        instance = my_class()
        return instance

#     def load_graph_and_inputs(args):
#     from dispel4py.utils import load_graph

#     graph = load_graph(args.module, args.attr)
#     if graph is None:
#         return None, None

#     graph.flatten()
#     inputs = create_inputs(args, graph)
#     return graph, inputs

# def load_graph(graph_source, attr=None):
#     # try to load from a module
#     error_message = ''
#     try:
#         return loadGraph(graph_source, attr)
#     except ImportError:
#         # it's not a module
#         error_message += 'No module "%s"\n' % graph_source
#         pass
#     except Exception:
#         error_message += \
#             'Error loading graph module:\n%s' % traceback.format_exc()
#         pass

#     # maybe it's a file?
#     try:
#         return loadGraphFromFile('temp', graph_source, attr)
#     except IOError:
#         # it's not a file
#         error_message += 'No file "%s"\n' % graph_source
#     except Exception:
#         error_message += \
#             'Error loading graph from file:\n%s' % traceback.format_exc()

#     # we don't know what it is
#     print('Failed to load graph from "%s":\n%s' %
#           (graph_source, error_message))